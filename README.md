<h1 align="center"> Hi! I'm Sebastián Maciel </h1>
<h3 align="center"> Software Engineer & Tech Writer</h3>

### 💻 What I'm about these weeks =>

- On Javascript is my attention
- Chakra UI well versed user
- Loved the basic stuff about Go 
- Using a lot of React and the top libraries
- Doing React Native with Typescript
- Devote to Formik + Yup couple
- React Table & React Query turned out to be very useful
- NodeJS & Express | MongoDB & Mongoose | I have done stufff with all that backend amazing things
- Avid reader about HTML5, CSS3 & Sass cool stuff
- Sometime ago, make one app or two using PHP & MySQL
- On the way up, we go with: Netlify, Heroku & MongoAtlas, not so bad on these ones
- Using VS Code for almost everything
- Git & Jira is what I use to work with on a daily basis

### ⚡ Learning now ->

- Javascript Advanced topics
- React Native still in progress...

Human languages ->

- Spanish: Native
- English: B1
- Wanting to learn: Mandarin Chinese

Technical paperworks ->

- Tech Writer on [dev.to](https://dev.to/sebastianmaciel/)
- Educational Content Creator at [NotNini](https://notnini.com.ar/)

### A bit about me ->

- I was born in 1989, today living in Argentina.
- I like to learn technologies to do a better for me and others.
- I'm a fan of mixing virtual solutions for real life problems or opportunities.
- Working today as a Frontend Tech Lead in Tach Argentina.
